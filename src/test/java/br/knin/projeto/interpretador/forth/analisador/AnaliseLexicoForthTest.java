package br.knin.projeto.interpretador.forth.analisador;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.List;

class AnaliseLexicoForthTest {

    @ParameterizedTest
    @ValueSource(strings = {"1 2 3 + 4 *", ": dup2 dup dup ;"})
    void cadeiaDeveSerValida(final String cadeia) {
        Assertions.assertDoesNotThrow(() -> new LexicoForth().processar(List.of(cadeia)));
    }
}