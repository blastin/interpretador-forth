package br.knin.projeto.interpretador.forth.funcao;

import br.knin.projeto.interpretador.forth.literal.Numero;
import br.knin.projeto.interpretador.forth.maquina.Maquina;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.Arrays;
import java.util.stream.Collectors;

class ReducaoTest {

    @ParameterizedTest
    @CsvSource(value = {"1,2,+,3", "1,1,-,0", "100,20,*,2000", "1,1,dup,1 1 1"})
    void deveRealizarUmaOperacaoDeAdicao(final int a, final int b, final String funcao, final String expectativa) {

        final Maquina maquina =
                Maquina
                        .create()
                        .adiciona(new Numero(a))
                        .adiciona(new Numero(b))
                        .adiciona(new TabelaAgregadora().recupera(funcao));

        Assertions.assertEquals
                (
                        expectativa,
                        Arrays.stream(maquina.array())
                                .boxed()
                                .map(String::valueOf)
                                .collect(Collectors.joining(" "))
                );

        Assertions.assertTrue(maquina.vazia());

    }

}