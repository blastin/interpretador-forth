package br.knin.projeto.interpretador.forth.funcao;

import br.knin.projeto.interpretador.forth.Memoria;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class TabelaAgregadoraTest {

    @Test
    void quandoTentaRecuperarFuncaoNaoExistente() {

        final TabelaFuncional tabela = new TabelaAgregadora();

        Assertions.assertThrows
                (
                        IllegalArgumentException.class,
                        () -> tabela.recupera("!")
                );

    }

    @Test
    void quandoTentaRecuperarFuncaoExistente() {

        final TabelaFuncional tabela = new TabelaAgregadora();

        Assertions.assertDoesNotThrow
                (
                        () -> tabela.recupera("+")
                );

    }

    @Test
    void adicionaUmaNovaFuncaoERecupera() {

        Assertions.assertDoesNotThrow
                (
                        () ->

                                new TabelaAgregadora().adiciona("!", new Funcao() {
                                            @Override
                                            public void avaliar(final Memoria memoria) {
                                                // FAZ NADA
                                            }

                                            @Override
                                            public long paraInt() {
                                                return 0;
                                            }
                                        })
                                        .recupera("!")

                );
    }
}