package br.knin.projeto.interpretador.forth.maquina;

import br.knin.projeto.interpretador.forth.literal.Numero;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.Arrays;
import java.util.stream.Collectors;

class MaquinaForthTest {

    @ParameterizedTest
    @CsvSource(value = {"1,2,1 2"})
    void deveRetonarOsSeguintesValores(final int a, final int b, final String expectativa) {
        Assertions.assertEquals
                (
                        expectativa,
                        Arrays.stream
                                        (
                                                Maquina.create()
                                                        .adiciona(new Numero(a))
                                                        .adiciona(new Numero(b))
                                                        .array()
                                        )
                                .boxed()
                                .map(String::valueOf)
                                .collect(Collectors.joining(" "))
                );
    }
}