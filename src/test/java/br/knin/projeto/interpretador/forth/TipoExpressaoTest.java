package br.knin.projeto.interpretador.forth;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

class TipoExpressaoTest {

    @ParameterizedTest(name = "#{index} Chave {arguments} devem ser validos")
    @CsvSource(
            {
                    "1,true",
                    "20220, true",
                    "+,true", "-,true",
                    "/,true", "*, true",
                    "dup,true",
                    "!,false",
                    ": dup2 dup dup ;, true",
                    ": 1e da, false",
                    ": ** 2 *, false",
                    ": ** 2 * ;, true",
                    "1 2 3 + 4 *,false",
                    ": f X Y ;,true",
                    "f,true"
            }
    )
    void avaliarChaves(final String entrada, final boolean expectativa) {
        Assertions.assertEquals(expectativa, TipoExpressao.cadeiaValida(entrada));
    }

    @ParameterizedTest(name = "#{index} Chave {arguments} devem ser nao reconhecidos")
    @ValueSource(strings = {"!"})
    void deveSubirExcecaoPorNaoReconhecerTipo(final String entrada) {
        Assertions.assertThrows(IllegalArgumentException.class, () -> TipoExpressao.from(entrada));
    }

    @ParameterizedTest(name = "#{index} Chave {arguments} devem ser validos")
    @CsvSource({"1,NUMERO", "+,PALAVRA", "-,PALAVRA", "/,PALAVRA", "*, PALAVRA", "dup,PALAVRA"})
    void deveRetonarTipo(final String entrada, final String expectativa) {
        Assertions.assertEquals(TipoExpressao.valueOf(expectativa), TipoExpressao.from(entrada));
    }

}