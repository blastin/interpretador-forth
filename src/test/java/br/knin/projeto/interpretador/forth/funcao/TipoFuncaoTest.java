package br.knin.projeto.interpretador.forth.funcao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class TipoFuncaoTest {

    @ParameterizedTest
    @CsvSource({"+,ADICAO", "-,SUBTRACAO", "/,DIVISAO", "*, MULTIPLICACAO", "dup, DUP"})
    void deveRetornarUmTipoDeFuncao(final String entrada, final String expectativa) {
        Assertions.assertEquals(TipoFuncao.valueOf(expectativa), TipoFuncao.from(entrada));
    }

    @Test
    void deveSubirExcecao() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> TipoFuncao.from("asddsdsa"));
    }

}