package br.knin.projeto.interpretador.forth;

import br.knin.projeto.interpretador.forth.analisador.AnalisadorSemantico;
import br.knin.projeto.interpretador.forth.analisador.LexicoForth;
import br.knin.projeto.interpretador.forth.analisador.SemanticoForth;
import br.knin.projeto.interpretador.forth.parse.ParseDefinicaoImpl;
import br.knin.projeto.interpretador.forth.parse.ParseForth;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

class InterpretadorTest {

    @ParameterizedTest
    @CsvSource(
            {
                    "1 2 +, 3",
                    "1 2 + 4 *, 12",
                    "1 2 + 4 * dup, 12 12",
                    "10 2 /, 5",
                    ": dup2 dup dup ;| 1 dup2, 1 1 1",
                    ": soma-estranha 1 + ;|: dup2 dup dup * ;| 1 soma-estranha 4 dup2, 128",
            }
    )
    void deveInterpretarASeguinteCadeia(final String cadeia, final String resposta) {

        final Interpretador interpretador = new InterpretadorForth
                (
                        new LexicoForth(),
                        AnalisadorSemantico.criar(),
                        new ParseForth(new ParseDefinicaoImpl(new SemanticoForth()))
                );

        final List<String> cadeias = Arrays.stream(cadeia.split("\\|")).toList();

        final long[] valores = interpretador.processar(cadeias);

        Assertions.assertEquals
                (
                        resposta,
                        Arrays.stream(valores)
                                .boxed()
                                .map(String::valueOf)
                                .collect(Collectors.joining(" "))
                );

    }

}