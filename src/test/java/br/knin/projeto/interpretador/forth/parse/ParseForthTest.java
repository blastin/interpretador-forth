package br.knin.projeto.interpretador.forth.parse;

import br.knin.projeto.interpretador.forth.Expressao;
import br.knin.projeto.interpretador.forth.analisador.SemanticoForth;
import br.knin.projeto.interpretador.forth.funcao.Funcao;
import br.knin.projeto.interpretador.forth.literal.Numero;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.List;

class ParseForthTest {

    @Test
    void deveRealizarParseNumero() {

        final Collection<Expressao> expressoes =
                new ParseForth(null)
                        .processar(new SemanticoForth().processar(List.of("1")));

        Assertions.assertFalse(expressoes.isEmpty());

        final Expressao expressao = expressoes.stream().findFirst().get();

        Assertions.assertTrue(expressao instanceof Numero);

    }


    @Test
    void deveRealizarParsePalavra() {

        final Collection<Expressao> expressoes =
                new ParseForth(null).processar(new SemanticoForth().processar(List.of("dup")));

        Assertions.assertFalse(expressoes.isEmpty());

        final Expressao expressao = expressoes.stream().findFirst().get();

        Assertions.assertTrue(expressao instanceof Funcao);

    }

}