package br.knin.projeto.interpretador.forth.analisador;

import br.knin.projeto.interpretador.forth.Simbolo;
import br.knin.projeto.interpretador.forth.TipoExpressao;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

class SemanticoForthTest {

    @ParameterizedTest
    @CsvSource(
            {
                    ": qw 2 ;, 2 qw, 3",
                    ": ** 2 * ;, 1 + 2 swap drop, 6",
            }
    )
    void deveConstruirUmaColecaoSimbolica(final String A, final String B, final int quantidade) {
        final Collection<Simbolo> simbolos = Assertions.assertDoesNotThrow
                (
                        () -> new SemanticoForth()
                                .processar(List.of(A, B))
                );

        Assertions.assertEquals(quantidade, simbolos.size());

    }

    @ParameterizedTest
    @CsvSource(
            {
                    "1 2 3 4,: dup2 dup dup ;, 5", // DEFINIÇÕES DEVEM FICAR NO INICIO DA CADEIA
                    ": ** 2 * ;,: dup2 dup dup ;,7", // APENAS DEFINIÇÕES
                    "20220, 124", // NÃO É PERMITIDO DUAS CADEIAS DE PROCESSAMENTO
                    "dup, dup dup", // NÃO É PERMITIDO DUAS CADEIAS DE PROCESSAMENTO
                    ": dup2 dup dup ;,: exp 2 + 2 * ;", // APENAS DEFINIÇÕES
                    "1 2 3 + 4 *, 1 2 * 4 -", // NÃO É PERMITIDO DUAS CADEIAS DE PROCESSAMENTO
            }
    )
    void deveSubirExcecoes(final String A, final String B) {

        final AnalisadorSemantico simbolicoForth = AnalisadorSemantico.criar();

        final List<String> objetos = List.of(A, B);

        Assertions.assertThrows
                        (
                                IllegalArgumentException.class,
                                () -> simbolicoForth.processar(objetos)
                        )
                .printStackTrace();

    }


    @ParameterizedTest
    @CsvSource({"1,NUMERO", "+,PALAVRA", "-,PALAVRA", "/,PALAVRA", "*, PALAVRA", "dup, PALAVRA"})
    void deveConstruirUmSimbolo(final String entrada, final String tipo) {

        final Collection<Simbolo> simbolos = AnalisadorSemantico.criar().processar(List.of(entrada));

        Assertions.assertFalse(simbolos.isEmpty());

        final Optional<Simbolo> first = simbolos.stream().findFirst();

        final Simbolo simbolo = first.get();

        Assertions.assertEquals(TipoExpressao.valueOf(tipo), simbolo.tipo());

        Assertions.assertEquals(entrada, simbolo.cadeia());

    }
}