package br.knin.projeto.interpretador.forth.parse;

import br.knin.projeto.interpretador.forth.Expressao;
import br.knin.projeto.interpretador.forth.Simbolo;
import br.knin.projeto.interpretador.forth.TipoExpressao;
import br.knin.projeto.interpretador.forth.funcao.TabelaAgregadora;
import br.knin.projeto.interpretador.forth.funcao.TabelaFuncional;
import br.knin.projeto.interpretador.forth.literal.Numero;

import java.util.ArrayList;
import java.util.Collection;

public final class ParseForth implements Parse {

    public ParseForth(final ParseDefinicao parseDefinicao) {
        this.parseDefinicao = parseDefinicao;
    }

    private final ParseDefinicao parseDefinicao;

    @Override
    public Collection<Expressao> processar(final Collection<Simbolo> simbolos) {

        final TabelaFuncional tabelaFuncional = new TabelaAgregadora();

        final Collection<Expressao> expressoes = new ArrayList<>();

        for (final Simbolo simbolo : simbolos) {

            if (simbolo.tipo().equals(TipoExpressao.NUMERO)) {
                expressoes.add(new Numero(Integer.parseInt(simbolo.cadeia())));
            } else if (simbolo.tipo().equals(TipoExpressao.PALAVRA)) {
                expressoes.add(tabelaFuncional.recupera(simbolo.cadeia()));
            } else if (simbolo.tipo().equals(TipoExpressao.DEFINICAO)) {
                final ParseDefinicao.Definicao definicao = parseDefinicao.processar(simbolo, tabelaFuncional);
                tabelaFuncional.adiciona(definicao.identificador(), definicao.funcao());
            } else throw new IllegalCallerException("Não implementado");

        }

        return expressoes;

    }

}
