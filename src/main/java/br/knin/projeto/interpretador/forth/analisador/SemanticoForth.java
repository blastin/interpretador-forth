package br.knin.projeto.interpretador.forth.analisador;

import br.knin.projeto.interpretador.forth.Simbolo;
import br.knin.projeto.interpretador.forth.TipoExpressao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.function.Function;
import java.util.function.Predicate;

public final class SemanticoForth implements AnalisadorSemantico {

    private static final String DELIMITADOR_FORTH = " ";

    @Override
    public Collection<Simbolo> processar(final Collection<String> cadeias) {

        final Collection<Simbolo> simbolos = new ArrayList<>();

        Function<String, Simbolo> definicao = SemanticoForth::paraSimbolo;

        for (final String cadeia : cadeias) {

            if (TipoExpressao.DEFINICAO.valida(cadeia)) {

                simbolos.add(definicao.apply(cadeia));

            } else {

                simbolos.addAll
                        (
                                Arrays.stream(cadeia.split(DELIMITADOR_FORTH))
                                        .filter(Predicate.not(String::isBlank))
                                        .map(SemanticoForth::paraSimbolo)
                                        .toList()
                        );

                definicao = SemanticoForth::chamadaIllegal;

            }

        }

        return simbolos;

    }

    private static Simbolo chamadaIllegal(final String cadeia) {
        throw new IllegalArgumentException("Error: Definições devem ficar no inicio da cadeia");
    }

    private static Simbolo paraSimbolo(final String s) {

        return new Simbolo() {

            @Override
            public TipoExpressao tipo() {
                return TipoExpressao.from(s);
            }

            @Override
            public String cadeia() {
                return s;
            }

        };

    }


}
