package br.knin.projeto.interpretador.forth;

import br.knin.projeto.interpretador.forth.analisador.AnalisadorSemantico;
import br.knin.projeto.interpretador.forth.analisador.AnaliseLexico;
import br.knin.projeto.interpretador.forth.maquina.Maquina;
import br.knin.projeto.interpretador.forth.parse.Parse;

import java.util.Collection;

public final class InterpretadorForth implements Interpretador {

    public InterpretadorForth
            (
                    final AnaliseLexico analiseLexico,
                    final AnalisadorSemantico analisadorSemantico,
                    final Parse parse
            ) {
        this.analiseLexico = analiseLexico;
        this.analisadorSemantico = analisadorSemantico;
        this.parse = parse;
    }

    private final AnaliseLexico analiseLexico;

    private final Parse parse;

    private final AnalisadorSemantico analisadorSemantico;

    @Override
    public long[] processar(final Collection<String> cadeias) {

        //ANALISE SINTATICA
        analiseLexico.processar(cadeias); // throws IllegalArgumentException

        // ESTRUTURAS SIMBOLICA
        final Collection<Simbolo> simbolos = analisadorSemantico.processar(cadeias);

        //TABELA DE EXPRESSOES
        final Collection<Expressao> expressoes = parse.processar(simbolos);

        // MAQUINA DE PROCESSAMENTO
        final Maquina maquina = Maquina.create(expressoes);

        return maquina.array();

    }
}
