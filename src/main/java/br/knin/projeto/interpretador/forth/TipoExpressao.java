package br.knin.projeto.interpretador.forth;

import java.util.Arrays;
import java.util.function.Predicate;
import java.util.regex.Pattern;

public enum TipoExpressao {

    NUMERO(Pattern.compile("\\d+").asMatchPredicate()),
    PALAVRA(Pattern.compile("^([+\\-*/]|[a-zA-Z]+[a-zA-Z0-9]?([\\-_]?[0-9a-zA-Z]+)?)$").asMatchPredicate()),
    DEFINICAO(Pattern.compile("^(:\\s+[\\w+\\-/*]+)\\s+([\\w+\\-/*]+\\s+)+;$").asMatchPredicate());

    static boolean cadeiaValida(final String cadeia) {
        return Arrays.stream(values()).anyMatch(e -> e.regular.test(cadeia));
    }

    private static final String MSG_ERROR = "Error: chave '%s' não pode ser reconhecida pelo interpretador";

    public static TipoExpressao from(final String chave) {
        return Arrays
                .stream(values()).filter(tipoExpressao -> tipoExpressao.regular.test(chave))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(String.format(MSG_ERROR, chave)));
    }

    TipoExpressao(final Predicate<String> regular) {
        this.regular = regular;
    }

    private final Predicate<String> regular;

    public boolean valida(final String cadeia) {
        return regular.test(cadeia);
    }

}
