package br.knin.projeto.interpretador.forth.maquina;

import br.knin.projeto.interpretador.forth.Expressao;

import java.util.Collection;

public sealed interface Maquina permits MaquinaForth {

    static Maquina create() {
        return new MaquinaForth();
    }

    static Maquina create(final Collection<Expressao> expressoes) {
        final Maquina maquinaForth = create();
        expressoes.forEach(maquinaForth::adiciona);
        return maquinaForth;
    }

    Maquina adiciona(final Expressao expressao);

    /**
     * Transforma todas as expressões em um array de inteiro
     *
     * @return Após remover todas as expressões da pilha, deverá ser retornado array de inteiros
     * @throws IllegalCallerException caso as expressões não seja númericas a exceção abaixo poderá ser lançada
     */
    long[] array() throws IllegalCallerException;

    boolean vazia();

}
