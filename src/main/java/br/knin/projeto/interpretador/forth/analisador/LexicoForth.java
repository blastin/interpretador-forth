package br.knin.projeto.interpretador.forth.analisador;

import java.util.Collection;
import java.util.function.Predicate;
import java.util.regex.Pattern;

public final class LexicoForth implements AnaliseLexico {

    private static final String MSG_ERROR = "Error: Cadeia '%s' invalida";

    private static final Predicate<String> REGEX = Pattern.compile("^[\\w;\\-+*/\\s:]+$").asMatchPredicate();

    @Override
    public void processar(final Collection<String> cadeias) throws IllegalArgumentException {
        cadeias
                .stream()
                .filter(Predicate.not(REGEX))
                .forEach
                        (
                                linha -> {
                                    throw new IllegalArgumentException
                                            (
                                                    String.format(MSG_ERROR, linha)
                                            );
                                }
                        );
    }
}
