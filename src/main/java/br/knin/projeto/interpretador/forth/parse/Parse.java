package br.knin.projeto.interpretador.forth.parse;

import br.knin.projeto.interpretador.forth.Expressao;
import br.knin.projeto.interpretador.forth.Simbolo;

import java.util.Collection;

public sealed interface Parse permits ParseForth {

    Collection<Expressao> processar(final Collection<Simbolo> simbolos);

}
