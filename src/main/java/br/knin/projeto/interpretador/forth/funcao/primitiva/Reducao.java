package br.knin.projeto.interpretador.forth.funcao.primitiva;

import br.knin.projeto.interpretador.forth.Expressao;
import br.knin.projeto.interpretador.forth.Memoria;
import br.knin.projeto.interpretador.forth.funcao.Funcao;
import br.knin.projeto.interpretador.forth.literal.Numero;

import java.util.Deque;
import java.util.function.BinaryOperator;

public final class Reducao implements Funcao {

    public Reducao(final BinaryOperator<Long> operator) {
        this.operator = operator;
    }

    private final BinaryOperator<Long> operator;

    @Override
    public void avaliar(final Memoria memoria) {

        final Deque<Expressao> colecao = memoria.todas();

        if (colecao.size() < 2)
            throw new IllegalArgumentException("Para realizar redução é necessario pelo menos dois valores inteiros");

        memoria.armazena(new Numero(colecao.stream().map(Expressao::paraInt).reduce(operator).orElseThrow()));

    }

}
