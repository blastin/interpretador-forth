package br.knin.projeto.interpretador.forth.funcao.primitiva;

import br.knin.projeto.interpretador.forth.Expressao;
import br.knin.projeto.interpretador.forth.Memoria;
import br.knin.projeto.interpretador.forth.funcao.Funcao;

import java.util.List;

public final class Dup implements Funcao {

    @Override
    public void avaliar(final Memoria memoria) {
        final Expressao expressao = memoria.ultima();
        memoria.armazena(List.of(expressao, expressao));
    }


}
