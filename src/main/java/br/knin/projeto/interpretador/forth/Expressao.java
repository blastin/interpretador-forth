package br.knin.projeto.interpretador.forth;

public interface Expressao {

    void avaliar(final Memoria memoria);

    default long paraInt() {
        throw new IllegalCallerException("Expressao não implementa");
    }
}
