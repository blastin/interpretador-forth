package br.knin.projeto.interpretador.forth.parse;

import br.knin.projeto.interpretador.forth.Expressao;
import br.knin.projeto.interpretador.forth.Simbolo;
import br.knin.projeto.interpretador.forth.TipoExpressao;
import br.knin.projeto.interpretador.forth.analisador.AnalisadorSemantico;
import br.knin.projeto.interpretador.forth.funcao.TabelaFuncional;
import br.knin.projeto.interpretador.forth.literal.Numero;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Collection;
import java.util.Deque;

public final class ParseDefinicaoImpl implements ParseDefinicao {

    public ParseDefinicaoImpl(final AnalisadorSemantico analisadorSemantico) {
        this.analisadorSemantico = analisadorSemantico;
    }

    private final AnalisadorSemantico analisadorSemantico;

    @Override
    public Definicao processar(final Simbolo simbolo, final TabelaFuncional tabelaFuncional) {

        final String objeto = simbolo.cadeia()
                .replace(":", "").replace(";", "").trim();

        final Deque<String> palavras = new ArrayDeque<>(Arrays.asList(objeto.split(" ")));

        final String identificador = palavras.removeFirst();

        if (TipoExpressao.NUMERO.valida(identificador))
            throw new IllegalArgumentException(String.format("Error: o valor numérico '%s' não pode ser utilizado como identificador", identificador));

        final Deque<Expressao> expressoes = new ArrayDeque<>();

        final Collection<Simbolo> simbolos = analisadorSemantico.processar(palavras);

        for (final Simbolo novoSimbolo : simbolos) {

            if (novoSimbolo.tipo().equals(TipoExpressao.NUMERO)) {
                expressoes.add(new Numero(Integer.parseInt(novoSimbolo.cadeia())));
            } else if (novoSimbolo.tipo().equals(TipoExpressao.PALAVRA)) {
                expressoes.add(tabelaFuncional.recupera(novoSimbolo.cadeia()));
            }

        }

        return new Definicao
                (
                        identificador,
                        maquina -> expressoes.forEach(expressao -> expressao.avaliar(maquina))
                );

    }

}
