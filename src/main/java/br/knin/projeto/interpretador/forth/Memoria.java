package br.knin.projeto.interpretador.forth;

import java.util.Collection;
import java.util.Deque;

public interface Memoria {

    /**
     * Coleção de expressões
     *
     * @return expressões serão removidas da máquina, quando essa operação é chamada
     */
    Deque<Expressao> todas();

    Expressao ultima();

    /**
     * Expressão será adicionada na máquina
     *
     * @param expressao expressão teoricamente avaliada
     */
    void armazena(final Collection<Expressao> expressao);

    /**
     * Expressão será adicionada na máquina
     *
     * @param expressao expressão teoricamente avaliada
     */
    void armazena(final Expressao expressao);

}
