package br.knin.projeto.interpretador.forth.funcao;

import br.knin.projeto.interpretador.forth.funcao.primitiva.Divisao;
import br.knin.projeto.interpretador.forth.funcao.primitiva.Dup;
import br.knin.projeto.interpretador.forth.funcao.primitiva.Reducao;

import java.util.Arrays;

public enum TipoFuncao {

    ADICAO("+") {
        @Override
        protected Funcao instancia() {
            return new Reducao(Long::sum);
        }
    },

    DIVISAO("/") {
        @Override
        protected Funcao instancia() {
            return new Divisao();
        }
    },

    MULTIPLICACAO("*") {
        @Override
        protected Funcao instancia() {
            return new Reducao((a, b) -> a * b);
        }
    },

    SUBTRACAO("-") {
        @Override
        protected Funcao instancia() {
            return new Reducao((a, b) -> a - b);
        }
    },
    DUP("dup") {
        @Override
        protected Funcao instancia() {
            return new Dup();
        }
    };

    private static final String MSG_ERROR = "Error: função '%s' não foi encontrada";

    static TipoFuncao from(final String chave) {
        return Arrays.stream(values())
                .filter(e -> e.identificador.equalsIgnoreCase(chave)).findFirst()
                .orElseThrow(() -> new IllegalArgumentException(String.format(MSG_ERROR, chave)));
    }

    TipoFuncao(final String identificador) {
        this.identificador = identificador;
    }

    private final String identificador;

    protected abstract Funcao instancia();

}
