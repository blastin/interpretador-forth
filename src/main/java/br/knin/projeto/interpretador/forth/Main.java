package br.knin.projeto.interpretador.forth;

import br.knin.projeto.interpretador.forth.analisador.AnalisadorSemantico;
import br.knin.projeto.interpretador.forth.analisador.LexicoForth;
import br.knin.projeto.interpretador.forth.analisador.SemanticoForth;
import br.knin.projeto.interpretador.forth.parse.ParseDefinicaoImpl;
import br.knin.projeto.interpretador.forth.parse.ParseForth;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) throws IOException {

        final Interpretador interpretador = new InterpretadorForth
                (
                        new LexicoForth(),
                        AnalisadorSemantico.criar(),
                        new ParseForth(new ParseDefinicaoImpl(new SemanticoForth()))
                );

        interpretar(args[0], interpretador);

    }

    private static void interpretar(final String nomeArquivo, final Interpretador interpretador) throws IOException {

        try (BufferedReader reader = new BufferedReader(new FileReader(nomeArquivo))) {

            final long[] resultado = interpretador.processar(reader.lines().toList());

            System.out.printf
                    (
                            "[Arquivo:%s] -> Saída: %s",
                            nomeArquivo,
                            Arrays.stream(resultado)
                                    .boxed()
                                    .map(String::valueOf)
                                    .collect(Collectors.joining(" "))
                    );

        }

    }
}
