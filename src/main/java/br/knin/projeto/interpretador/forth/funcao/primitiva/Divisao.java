package br.knin.projeto.interpretador.forth.funcao.primitiva;

import br.knin.projeto.interpretador.forth.Expressao;
import br.knin.projeto.interpretador.forth.Memoria;
import br.knin.projeto.interpretador.forth.funcao.Funcao;
import br.knin.projeto.interpretador.forth.literal.Numero;

import java.util.Deque;

public final class Divisao implements Funcao {

    @Override
    public void avaliar(final Memoria memoria) {

        final Deque<Expressao> expressoes = memoria.todas();

        if (expressoes.size() != 2)
            throw new IllegalArgumentException("Error: Para realizar divisão é necessario ter exatamente duas expressoes");

        final Expressao ultima = expressoes.removeLast();

        if (ultima.paraInt() == 0)
            throw new IllegalArgumentException("Error: Não é possível dividir por zero");

        final Expressao penultima = expressoes.removeLast();

        memoria.armazena(new Numero(penultima.paraInt() / ultima.paraInt()));

    }

}
