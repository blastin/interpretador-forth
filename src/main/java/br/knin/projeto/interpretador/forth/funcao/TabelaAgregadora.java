package br.knin.projeto.interpretador.forth.funcao;

import java.util.Map;
import java.util.TreeMap;

public final class TabelaAgregadora implements TabelaFuncional {

    public TabelaAgregadora() {
        this.funcoes = new TreeMap<>();
    }

    private final Map<String, Funcao> funcoes;

    @Override
    public TabelaFuncional adiciona(final String identificador, final Funcao funcao) {
        funcoes.put(identificador, funcao);
        return this;
    }

    @Override
    public Funcao recupera(final String funcao) {
        return funcoes.computeIfAbsent(funcao, s -> TipoFuncao.from(s).instancia());
    }

}
