package br.knin.projeto.interpretador.forth.parse;

import br.knin.projeto.interpretador.forth.Simbolo;
import br.knin.projeto.interpretador.forth.funcao.Funcao;
import br.knin.projeto.interpretador.forth.funcao.TabelaFuncional;

public interface ParseDefinicao {

    Definicao processar(final Simbolo simbolo, final TabelaFuncional tabelaFuncional);

    record Definicao(String identificador, Funcao funcao) {
    }
}
