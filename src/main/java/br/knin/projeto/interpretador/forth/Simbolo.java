package br.knin.projeto.interpretador.forth;

public interface Simbolo {

    TipoExpressao tipo();

    String cadeia();

}
