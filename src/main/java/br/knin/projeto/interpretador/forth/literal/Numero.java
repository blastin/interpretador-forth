package br.knin.projeto.interpretador.forth.literal;

import br.knin.projeto.interpretador.forth.Expressao;
import br.knin.projeto.interpretador.forth.Memoria;

public final class Numero implements Expressao {

    public Numero(final long valor) {
        this.valor = valor;
    }

    private final long valor;

    @Override
    public void avaliar(final Memoria memoria) {
        memoria.armazena(this);
    }

    @Override
    public long paraInt() {
        return valor;
    }

}
