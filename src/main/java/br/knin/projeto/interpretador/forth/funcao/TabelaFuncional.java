package br.knin.projeto.interpretador.forth.funcao;

/**
 * Todas funções deverão ser armazenadas nessa tabela para facilitar a verificação semântica e também
 * encontrar sua instância
 */
public interface TabelaFuncional {

    /**
     * Adiciona uma função na tabela
     *
     * @param funcao função expressão
     * @return propria instancia da tabela
     */
    TabelaFuncional adiciona(final String identificador, final Funcao funcao);

    /**
     * @param funcao cadeia de caracteres que possibilita identificar uma função
     * @return Caso a função informada exista será retornado uma instância
     */
    Funcao recupera(final String funcao);

}
