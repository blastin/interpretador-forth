package br.knin.projeto.interpretador.forth.maquina;

import br.knin.projeto.interpretador.forth.Expressao;
import br.knin.projeto.interpretador.forth.Memoria;

import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Deque;

final class MaquinaForth implements Maquina, Memoria {

    MaquinaForth() {
        armanezamento = new ArrayDeque<>();
    }

    private final Deque<Expressao> armanezamento;

    @Override
    public Maquina adiciona(final Expressao expressao) {
        expressao.avaliar(this);
        return this;
    }

    @Override
    public long[] array() throws IllegalCallerException {

        return todas()
                .stream()
                .mapToLong(Expressao::paraInt).toArray();
    }

    @Override
    public boolean vazia() {
        return armanezamento.isEmpty();
    }

    @Override
    public Deque<Expressao> todas() {
        final Deque<Expressao> expressoes = new ArrayDeque<>(armanezamento);
        armanezamento.clear();
        return expressoes;
    }

    @Override
    public Expressao ultima() {
        return armanezamento.removeLast();
    }

    @Override
    public void armazena(final Collection<Expressao> expressoes) {
        armanezamento.addAll(expressoes);
    }

    @Override
    public void armazena(final Expressao expressao) {
        armanezamento.add(expressao);
    }

}
