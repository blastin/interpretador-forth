package br.knin.projeto.interpretador.forth.analisador;

import java.util.Collection;

public sealed interface AnaliseLexico permits LexicoForth {

    void processar(final Collection<String> instrucoes) throws IllegalArgumentException;

}
