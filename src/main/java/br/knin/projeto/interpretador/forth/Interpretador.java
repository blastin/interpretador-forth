package br.knin.projeto.interpretador.forth;

import java.util.Collection;

public interface Interpretador {
    long[] processar(Collection<String> cadeias);

}
