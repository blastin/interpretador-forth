package br.knin.projeto.interpretador.forth.analisador;

import br.knin.projeto.interpretador.forth.Simbolo;

import java.util.Collection;

public sealed interface AnalisadorSemantico permits AnalisadorSemanticoProxy, SemanticoForth {

    static AnalisadorSemantico criar() {
        return new AnalisadorSemanticoProxy(new SemanticoForth());
    }

    Collection<Simbolo> processar(final Collection<String> cadeias);

}
