package br.knin.projeto.interpretador.forth.analisador;

import br.knin.projeto.interpretador.forth.Simbolo;
import br.knin.projeto.interpretador.forth.TipoExpressao;

import java.util.Collection;

final class AnalisadorSemanticoProxy implements AnalisadorSemantico {

    public AnalisadorSemanticoProxy(final AnalisadorSemantico analisadorSemantico) {
        this.analisadorSemantico = analisadorSemantico;
    }

    private final AnalisadorSemantico analisadorSemantico;

    @Override
    public Collection<Simbolo> processar(final Collection<String> cadeias) {

        final long diferenca = cadeias.size() - cadeias.stream().filter(TipoExpressao.DEFINICAO::valida).count();

        if (diferenca == 0)
            throw new IllegalArgumentException("Error: Não será possível interpretar apenas definições");

        if (diferenca > 1)
            throw new IllegalArgumentException("Error: Não será possível interpretar mais do que uma linha de instruções");

        return analisadorSemantico.processar(cadeias);

    }

}
